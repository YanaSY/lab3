#include <math.h>
#include <stdio.h>
double summ (int n)
{
	double summ = 0; 
	for (int i = 0; i < n; ++i)
	{
		summ += powl((-1), i) * (pow(i, 2) + 1) / (pow(i, 3) + 3);
	}
	return summ;
}
