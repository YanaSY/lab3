#include <math.h>
#include <stdio.h>
int findFirstElement (double exp)
{
	double summ = 0;
	int i;
		for (int i = 0; ; ++i)
		{
			summ = powl((-1), i) * (pow(i, 2) + 1) / (pow(i, 3) + 3);
			if (summ < 0)
			{
				if ((fabs(summ) <= exp))
					return (i);
			}
		}
	return (-1);
}
