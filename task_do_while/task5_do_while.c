#include <math.h>
#include <stdio.h>
int findFirstNegativElement (double exp) 
{
	double summ = 0;
	int i = 0;
	do 
	{
		summ = powl((-1), i) * (pow(i, 2) + 1) / (pow(i, 3) + 3);
		if (summ < 0)
		{
			if ((fabs(summ) <= exp))
				return (i);
		}
		++i;
		
	} while (1);
	return (-1);
}
