#include <math.h>
#include <stdio.h>
void main(void)
{
	int i;
	int k;
	int n;
	double eps;
	printf("1 - Task1\n");
	printf("2 - Task2\n");
	printf("3 - Task3\n");
	printf("4 - Task4\n");
	printf("5 - Task5\n");
	printf("Input number task: \n");
	scanf("%d", &i);
	switch (i)
	{
	case '1':
	{
		printf("Input n: ");
		scanf("%d", &n);
		printf("Output: %lf\n", summ(n));
		break;
	}
	case '2':
	{
		printf("Input eps: ");
		scanf("%lf", &eps);
		printf("Output: %lf\n", summ2(eps));
		break;
	}
	case '3':
	{
		printf("Input n: ");
		scanf("%d", &n);
		printf("Input k: ");
		scanf("%d", &k);
		print(n, k);
		break;
	}
	case '4':
	{
		printf("Input eps: ");
		scanf("%lf", &eps);
		findFirstElement(eps);
		break;
	}
	case '5':
	{
		printf("Input eps: ");
		scanf("%lf", &eps);
		findFirstNegativElement(eps);
		break;
	}
	case '6':
		return;
	default:
	{
		printf("Error input\n");
		break;
	}

	}
}
