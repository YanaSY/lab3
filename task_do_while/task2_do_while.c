#include <math.h>
#include <stdio.h>
double summ2 (double e)
{
	double x = 0;
	double summ = 0;
	int i = 0;
	do 
	{
		x = powl((-1), i) * (pow(i, 2) + 1) / (pow(i, 3) + 3);
		summ += powl((-1), i) * (pow(i, 2) + 1) / (pow(i, 3) + 3);
		++i;
	} while (fabs(x) <= e);
	return summ;
}
