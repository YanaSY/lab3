#include <math.h>
#include <stdio.h>
double summ (int n)
{
	double summ = 0;
	int i = 0;
	do 
	{
		summ += powl((-1), i) * (pow(i, 2) + 1) / (pow(i, 3) + 3);
		++i;
	} while (i < n - 1);
	return summ;
}
