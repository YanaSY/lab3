#include <math.h>
#include <stdio.h>
int findFirstElement (double eps) 
{
	double summ = 0;
	int i = 0;
	do 
	{
		summ = powl((-1), i) * (pow(i, 2) + 1) / (pow(i, 3) + 3);
		if ((fabs(summ) <= eps))
			break;
		++i;
		
	} while (1);
	return (i);
}
