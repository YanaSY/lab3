#include <math.h>
#include <stdio.h>
int findFirstElement(double exp)
{
	double summ ;
	int i = 0;
	while (1)
	{
		summ = powl((-1), i) * (pow(i, 2) + 1) / (pow(i, 3) + 3);
		if ((fabs(summ) <= exp))
			break;
		++i;
	}
	return i;
}
