#include <math.h>
#include <stdio.h>
void print (int n, int k)
{
	double summ ;
	int i = 0;
	while (i < (n+1))
	{
		if ((i % k) == 0)
		{
			++i;
			continue;
		}
		summ += powl((-1), i) * (pow(i, 2) + 1) / (pow(i, 3) + 3);
		++i;
	}
	return 0;
}
